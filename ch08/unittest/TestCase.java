import java.util.Objects;

public interface TestCase
{
   void execute() throws Exception;
   default void assertEquals(Object expected, Object actual)
   {
      if (!Objects.equals(actual, expected))
         throw new TestException("Expected: " + expected + "\nActual: " + actual);
   }
}
