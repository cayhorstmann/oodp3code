import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class LongWordFinder
{
   private static String LAST = " LAST ";
   
   public static Runnable findLongWords(
      String filename, int minLength, BlockingQueue<String> queue)
   {
      return () -> 
         {
            try
            {
               List<String> lines = Files.readAllLines(Paths.get(filename));
               for (String line : lines)
               {
                  String[] words = line.split("[\\PL]+");
                  for (String word : words)
                     if (word.length() >= minLength)
                        queue.put(word);
               }
               queue.put(LAST);
            }
            catch (InterruptedException | IOException ex)
            {
               // terminate task
            }
         };
   }

   public static void main(String[] args) throws Exception
   {
      String[] filenames = { 
            "../longwords/alice30.txt",
            "../longwords/war-and-peace.txt",
            "../longwords/crsto10.txt"
      };

      BlockingQueue<String> queue = new ArrayBlockingQueue<>(50);
      
      ExecutorService service = Executors.newCachedThreadPool();
      for (String filename : filenames)
      {
         service.submit(findLongWords(filename, 15, queue));
      }

      Runnable consumer = () ->
         {
            try
            {
               int completed = 0;
               Set<String> uniqueLongWords = new TreeSet<>();
               while (completed < filenames.length)
               {
                  String word = queue.take();
                  if (word == LAST) completed++;
                  else uniqueLongWords.add(word);
               }
               for (String word : uniqueLongWords)
               {
                  System.out.println(word);
               }
            }
            catch (InterruptedException ex)
            {
               // terminate task
            }
         };
      service.submit(consumer);
      service.shutdown();
   }
}
