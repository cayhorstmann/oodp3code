import java.util.concurrent.*;

public class ConcurrentGreetings
{
   public static Runnable producer(String greeting,
      BoundedQueue<String> q, int repetitions)
   {
      return () ->
         {
            try
            {
               for (int i = 0; i < repetitions; i++)
                  q.add(greeting);
            }
            catch (InterruptedException ex)
            {
               // terminate
            }               
         };
   }

   public static Runnable consumer(
      String s1, String s2,
      BoundedQueue<String> q, int n)
   {
      return () ->
         {
            try
            {
               int count1 = 0;
               int count2 = 0;
               for (int i = 0; i < n; i++)
               {
                  String s = q.remove();
                  if (s1.equals(s)) count1++;
                  else if (s2.equals(s)) count2++;
               }
               System.out.println(s1 + ": " + count1);
               System.out.println(s2 + ": " + count2);
            }
            catch (InterruptedException ex)
            {
               // terminate
            }
         };
   }

   public static void main(String[] args)
   {
      int n = 1000;
      BoundedQueue<String> q = new BoundedQueue<>(2 * n);
      ExecutorService service = Executors.newCachedThreadPool();
      Runnable p1 = producer("Hello", q, n);
      Runnable p2 = producer("Goodbye", q, n);
      Runnable c = consumer("Hello", "Goodbye", q, 2 * n);
      service.execute(p1);
      service.execute(p2);
      service.execute(c);
      service.shutdown();
   }
}
