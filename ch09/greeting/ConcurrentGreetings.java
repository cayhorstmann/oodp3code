import java.util.concurrent.*;

public class ConcurrentGreetings
{
   public static Runnable greeter(String greeting, int repetitions)
   {
      return () ->
         {
            for (int i = 0; i < repetitions; i++)
               System.out.println(greeting);
         };
   }

   public static void main(String[] args)
   {
      ExecutorService service = Executors.newCachedThreadPool();
      Runnable r1 = greeter("Hello", 100);
      Runnable r2 = greeter("Goodbye", 100);
      service.execute(r1);
      service.execute(r2);
      service.shutdown();
   }
}
